#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/")
public class EchoResource {

  @GET
  public String ola(){
    return "Funciona mesmo";
  }
}
