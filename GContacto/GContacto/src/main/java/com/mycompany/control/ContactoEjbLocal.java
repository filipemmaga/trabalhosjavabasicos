/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.control;

import com.mycompany.model.Contacto;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author CTW00170-Admin
 */
@Local
public interface ContactoEjbLocal {

    void create();

    List<Contacto> getAll();
    
}
