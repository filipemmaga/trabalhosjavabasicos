/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.control;

import com.mycompany.model.Contacto;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author CTW00170-Admin
 */
@Stateless
@Named
public class ContactoEjb implements ContactoEjbLocal {

    @PersistenceContext
    EntityManager em;
    
    @Inject Contacto ci;
    
    @Override
    public void create() {
        Contacto contactNew = new Contacto(ci.getName(), ci.getPhone());
        em.persist(contactNew);
    }

    @Override
    public List<Contacto> getAll() {
        return em.createNamedQuery("contacto.selectAll").getResultList();
    }

    
}
