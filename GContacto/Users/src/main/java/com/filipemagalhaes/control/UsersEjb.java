/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filipemagalhaes.control;

import com.filipemagalhaes.model.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author CTW00170-Admin
 */
@Stateless
@Named
public class UsersEjb implements UsersEjbLocal {
    @PersistenceContext
    EntityManager em;
    
    @Inject Users userInject;
    
    @Override
    public void create() {
        Users usersNew = new Users(userInject.getNameUser(), userInject.getAddress(), userInject.getPhone());
        em.persist(usersNew);
    }

    @Override
    public List<Users> getAll() {
        return em.createNamedQuery("users.selectAll").getResultList();
    }
}
